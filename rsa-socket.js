var crypto = require('origami-crypto-utils');
var debug = require('debug')('origami:rsa-socket');
var ConnectedEmitters = require('origami-connected-emitters');

function RSASocket(privateKey) {
  if (!privateKey) throw new Error('private key is required');

  this.id = Math.round(Math.random() * 1000);

  try {
    this.privateKey = crypto.asPrivateKey(privateKey);
  } catch (e) {
    throw new Error('invalid private key');
  }
}

RSASocket.prototype.connect = function (socket) {
  if (!socket) throw new Error('socket is required');

  var self = this;

  return new Promise(function (resolve, reject) {

    var connected = false;

    var listenRsaEvent = function (otherSidePublicKey) {
      var connectedEmitters = new ConnectedEmitters();
      
      var incoming = connectedEmitters.createEmitter();
      var outgoing = connectedEmitters.createEmitter();
    
      if (connected) return;

      connected = true;
      
      var forwarder = function () {
        if (arguments[0] === 'removeListenerAny') return;

        debug('%s: handling %s', self.id, arguments[0]);

        var plainArgs = [];

        for (var i = 0; i < arguments.length; i++) {
          if (typeof(arguments[i]) !== 'function') plainArgs.push(arguments[i]);
        }

        var encryptedArgs = crypto.encryptAndSign(
          plainArgs,
          self.privateKey,
          otherSidePublicKey
        );

        var callback;

        if (typeof(arguments[arguments.length - 1]) === 'function') {
          callback = arguments[arguments.length - 1];
        }

        var args = ['rsa-event', encryptedArgs];

        if (callback) {
          args.push(function (encryptedCallbackResponse) {
            var decryptedCallbackResponse = crypto.decryptAndVerify(
              encryptedCallbackResponse,
              self.privateKey,
              otherSidePublicKey
            );

            callback.apply(outgoing, decryptedCallbackResponse);
          });
        }

        debug('%s: forwarding encrypted %s', self.id, this.event);

        socket.emit.apply(socket, args);
      };

      socket
      .on(
        'rsa-event',
        function (encryptedEvent, callback) {
          if (arguments[0] === 'removeListenerAny') return;
          
          debug('%d: received rsa-event', self.id);

          var args = crypto.decryptAndVerify(
            encryptedEvent,
            self.privateKey,
            otherSidePublicKey
          );

          debug('%s: rsa-event decrypted as %s', self.id, args[0]);

          if (callback) {
            debug('%s: rsa-event %s has callback', self.id, args[0]);

            args.push(function () {
              debug('%s: secure socket %s callback called', self.id, args[0]);

              var callbackResponseArgs = [];
              for (var i = 0; i < arguments.length; i++) callbackResponseArgs.push(arguments[i]);

              var encrypted = crypto.encryptAndSign(
                callbackResponseArgs,
                self.privateKey,
                otherSidePublicKey
              );

              callback.call(socket, encrypted);
            });
          }

          debug('%s: emitting %s on secure socket', self.id, args[0]);

          incoming.fireEvent.apply(incoming, args);
        }
      );

      outgoing
      .onAny(
        forwarder
      );
      
      socket.on('disconnect', function () {
        outgoing.emit('disconnect');
      });

      resolve(incoming);
    };

    debug('%s: emitting rsa-hello', self.id);

    socket
    .on(
      'rsa-hello',
      function (otherSidePublicKey, callback) {
        debug('%s: rsa-hello received', self.id);

        callback(null, self.privateKey.exportKey('pkcs1-public-pem'));
        listenRsaEvent(otherSidePublicKey);
      }
    );

    socket
    .emit(
      'rsa-hello',
      self.privateKey.exportKey('pkcs1-public-pem'),
      function (err, otherSidePublicKey) {
        if (err) return reject(err);

        debug('%s: rsa-hello callback received', self.id);

        listenRsaEvent(otherSidePublicKey);
      }
    );
  });
};

module.exports = RSASocket;

var assert = require('assert');
var RSASocket = require('..');
var testData = require('./testData');
var crypto = require('origami-crypto-utils');
var SocketMock = require('./socket-io-mock');
var EventEmitter = require('events').EventEmitter;

describe('RSA Socket', function () {
  this.timeout(400);
  
  var mockIo;
  
  beforeEach(function () {
    mockIo = new SocketMock();
  });

  it('requires a private key to instantiate', function () {
    assert
    .throws(
      function () {
        new RSASocket();
      },
      /private key is required/
    );
  });

  it('rejects invalid private key', function () {
    assert
    .throws(
      function () {
        new RSASocket(
          'invalid private key'
        );
      },
      /invalid private key/
    );
  });

  it('accepts valid private key', function () {
    new RSASocket(
      testData.pair1.privateKey
    );
  });

  describe('.connect', function () {
    it('requires a socket', function () {
      var target = new RSASocket(testData.pair1.privateKey);

      assert
      .throws(
        function () {
          target.connect();
        },
        /socket is required/
      );
    });

    it('returns a Promise', function () {
      var target = new RSASocket(testData.pair1.privateKey);

      var fakeSocket = new EventEmitter();

      var promise = target.connect(fakeSocket);

      assert(promise instanceof Promise);
    });

    describe('rsa-hello', function () {
      it('emits rsa-hello', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          try {
            assert.equal(testData.pair1.publicKey, publicKey);
            assert(callback);

            done();
          } catch (e) {
            done(e);
          }
        });

        target.connect(fakeSocket);
      });

      it('accepts other side publicKey', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });

        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          done();
        });
      });
    });

    describe('rsa-event', function () {
      it('encrypts one side events', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });

        fakeSocket
        .on('rsa-event', function (args) {
          var decrypted = crypto
            .decryptAndVerify(
              args,
              testData.pair2.privateKey,
              testData.pair1.publicKey
            );

          assert(decrypted);
          assert.deepEqual(['my-event', 'my-arg'], decrypted);

          done();
        });

        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          securedSocket
          .emit('my-event', 'my-arg');
        });
      });

      it('handles callback if present', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });

        fakeSocket
        .on(
          'rsa-event',
          function (args, callback) {
            var encrypted = crypto.encryptAndSign(
              ['my-result'],
              testData.pair2.privateKey,
              testData.pair1.publicKey
            );

            callback(encrypted);
          }
        );

        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          securedSocket
          .emit(
            'my-event',
            'my-arg',
            function () {
              done();
            }
          );
        });
      });

      it('receives other side events', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });

        var encrypted = crypto.encryptAndSign(
          ['my-other-event', 'my-other-arg'],
          testData.pair2.privateKey,
          testData.pair1.publicKey
        );

        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          securedSocket
          .on(
            'my-other-event',
            function (arg1) {
              assert.equal('my-other-arg', arg1);
              done();
            }
          );

          fakeSocket.emit('rsa-event', encrypted);
        });
      });

      it('decrypts other side event callback result', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });

        var encryptedEvent = crypto.encryptAndSign(
          ['my-other-event'],
          testData.pair2.privateKey,
          testData.pair1.publicKey
        );

        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          securedSocket
          .on(
            'my-other-event',
            function (callback) {
              try {
                callback('arg1', 'arg2');
              } catch (e) {
                done(e);
              }
            }
          );

          fakeSocket
          .emit(
            'rsa-event',
            encryptedEvent,
            function (result) {
              try {
                var decrypted = crypto.decryptAndVerify(
                  result,
                  testData.pair2.privateKey,
                  testData.pair1.publicKey
                );

                assert.deepEqual(
                  ['arg1', 'arg2'],
                  decrypted
                );

                done();
              } catch (e) {
                done(e);
              }
            }
          );
        });
      });
      
      it('emits event inside an emit callback', function (done) {
        var rsa1 = new RSASocket(testData.pair1.privateKey);
        var rsa2 = new RSASocket(testData.pair2.privateKey);
        
        Promise.all([
          rsa1.connect(mockIo),
          rsa2.connect(mockIo.socketClient)
        ])
        .then(function (secureSockets) {
          var ss1 = secureSockets[0];
          var ss2 = secureSockets[1];
          
          ss2.on('e1', function (cb) {
            cb();
          });
          ss2.on('e2', function () {
            done();
          });
          ss1.emit('e1', function () {
            ss1.emit('e2');
          });
        });
      });
    });
    
    describe('#disconnect', function () {
      it('propagates disconnect', function (done) {
        var target = new RSASocket(testData.pair1.privateKey);
        var fakeSocket = new EventEmitter();

        fakeSocket
        .on('rsa-hello', function (publicKey, callback) {
          callback(null, testData.pair2.publicKey);
        });

        target
        .connect(fakeSocket)
        .then(function (securedSocket) {
          securedSocket
          .on(
            'disconnect',
            function () {
              done();
            }
          );

          fakeSocket.emit('disconnect');
        });
      });
    });
  });
});

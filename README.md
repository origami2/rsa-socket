# Origami RSA socket

### Purpose

Wrap socket messages on both ends to encrypt communication. Does not verify key provided by the other end.

### Usage

```javascript
var RSASocket = require('origami-rsa-socket');

var rsa1 = new RSASocket(
  key // private key is required. see https://gitlab.com/origami2/crypto-utils/blob/master/README.md
);

rsa1
.connect(
  socket // any socket that uses .emit/.on/.off like node EventEmitter, socket.io or EventEmitter2
)
.then(
  function (secureSocket) {
    // secureSocket is another socket like object.
    // messages will be encrypted, passed through, and decrypyted
    // on the other end
  }
);
```